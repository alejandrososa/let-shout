<?php
/**
 * Creado por PhpStorm.
 * Desarrollador: Alejandro Sosa <alesjohnson@hotmail.com>
 * Fecha: 21/1/18
 * Hora: 20:51
 */

namespace App\Tests\Twitter\Domain\Model;


use App\Twitter\Domain\Model\Tweet\Tweet;
use Faker\Factory;
use Faker\Generator;
use PHPUnit\Framework\TestCase;

class TweetTest extends TestCase
{
    /**
     * @var Generator
     */
    private $faker;

    protected function setUp()
    {
        $this->faker = Factory::create();
    }

    public function testCreateTweetSuccefull()
    {
        $data = [
            'id' => $this->faker->randomNumber(),
            'text' => $this->faker->text(280),
            'user' => [
                'screen_name' => 'myNewUsername',
                'profile_image_url' => $this->faker->imageUrl(),
            ],
            'created_at' => 'Thu Dec 10 04:04:22 +0000 2009',
        ];

        $tweet = Tweet::populateFromArray($data);
        $this->assertNotEmpty($tweet->getId());
    }
}
