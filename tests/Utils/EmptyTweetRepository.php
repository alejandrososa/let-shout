<?php
/**
 * Created by PhpStorm.
 * User: Alejandro Sosa <alesjohnson@hotmail.com>
 * Date: 24/01/18
 * Time: 15:31
 */

namespace App\Tests\Utils;


use App\Twitter\Domain\Model\Tweet\TweetRepository;
use App\Twitter\Domain\Model\Tweet\TweetUsername;

class EmptyTweetRepository implements TweetRepository
{
    /**
     * @param TweetUsername $username
     * @param int $total
     * @return array
     */
    public function tweetsByUsername(TweetUsername $username, int $total)
    {
        return [
            'errors' => [
                [
                    'code' => 34,
                    'message' => 'Sorry, that page does not exist.'
                ]
            ]
        ];
    }
}