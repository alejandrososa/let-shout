<?php
/**
 * Created by PhpStorm.
 * User: Alejandro Sosa <alesjohnson@hotmail.com>
 * Date: 2/01/18
 * Time: 13:48
 */

namespace App\Common\Application\Query;

/**
 * Interface CommandBus
 * @package App\Common\Application\Query
 */
interface QueryBus
{
    public function execute(Query $command);
}