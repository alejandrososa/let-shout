<?php
/**
 * Created by PhpStorm.
 * User: Alejandro Sosa <alesjohnson@hotmail.com>
 * Date: 2/01/18
 * Time: 14:46
 */

namespace App\Twitter\Application\Query\Tweet;

use App\Twitter\Application\Exception\UsernameIsEmptyException;
use App\Twitter\Domain\Model\Tweet\TweetRepository;
use App\Twitter\Domain\Model\Tweet\TweetUsername;
use App\Common\Application\Query\Query;
use App\Common\Application\Query\QueryHandler;
use App\Common\Application\Exception\WrongQueryException;

/**
 * Class TweetShoutQueryHandler
 * @package App\Twitter\Application\Query\Tweet
 */
class TweetShoutQueryHandler implements QueryHandler
{
    /**
     * @var TweetRepository
     */
    private $repository;

    /**
     * TweetShoutHandler constructor.
     * @param TweetRepository $repository
     */
    public function __construct(TweetRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param Query $command
     * @return mixed
     * @throws \Exception
     */
    public function handle(Query $command)
    {
        $this->guardCorrectCommand($command);
        $this->assertUsernameNotEmpty($command);

        /** @var TweetShoutQuery $command */
        $tweets = $this->repository->tweetsByUsername(
            new TweetUsername($command->getUsername()),
            $command->getMaxTweets()
        );

        return $tweets;
    }

    /**
     * @param Query $command
     * @throws WrongQueryException
     */
    private function guardCorrectCommand(Query $command): void
    {
        if (!$command instanceof TweetShoutQuery) {
            throw new WrongQueryException(self::class);
        }
    }

    /**
     * @param Query $command
     * @throws UsernameIsEmptyException
     */
    private function assertUsernameNotEmpty(Query $command)
    {
        /** @var TweetShoutQuery $command */
        if (empty($command->getUsername())) {
            throw new UsernameIsEmptyException('Username is empty');
        }
    }
}