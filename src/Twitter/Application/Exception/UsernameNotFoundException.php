<?php
/**
 * Created by PhpStorm.
 * User: Alejandro Sosa <alesjohnson@hotmail.com>
 * Date: 24/01/18
 * Time: 17:17
 */

namespace App\Twitter\Application\Exception;


class UsernameNotFoundException extends \Exception
{
}