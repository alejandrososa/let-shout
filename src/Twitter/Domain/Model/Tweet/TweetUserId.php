<?php
/**
 * Creado por PhpStorm.
 * Desarrollador: Alejandro Sosa <alesjohnson@hotmail.com>
 * Fecha: 21/1/18
 * Hora: 20:39
 */

namespace App\Twitter\Domain\Model\Tweet;

use App\Common\Domain\Model\ValueObjects\UuId;

final class TweetUserId extends UuId
{

}