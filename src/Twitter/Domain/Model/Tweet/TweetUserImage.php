<?php
/**
 * Created by PhpStorm.
 * User: Alejandro Sosa <alesjohnson@hotmail.com>
 * Date: 26/12/17
 * Time: 22:33
 */

namespace App\Twitter\Domain\Model\Tweet;

/**
 * Class TweetUserImage
 * @package App\Twitter\Domain\Model\Tweet
 */
class TweetUserImage
{
    /**
     * @var string
     */
    private $value;

    /**
     * UserName constructor.
     * @param $url
     */
    public function __construct(string $url)
    {
        $this->setValue($url);
    }

    public function __toString()
    {
        return $this->getValue();
    }

    public function getValue()
    {
        return $this->value;
    }

    private function setValue(string $url)
    {
        $this->assertNotEmpty($url);
        $this->assertIsValid($url);
        $this->value = trim($url);
    }

    private function assertNotEmpty($url)
    {
        if (empty($url)) {
            throw new \DomainException('Empty user name');
        }
    }

    private function assertIsValid($url)
    {
        if (!filter_var($url, FILTER_VALIDATE_URL)) {
            throw new \DomainException('Url of user image is not valid');
        }
    }
}