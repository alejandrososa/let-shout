<?php
/**
 * Created by PhpStorm.
 * User: Alejandro Sosa <alesjohnson@hotmail.com>
 * Date: 26/12/17
 * Time: 22:33
 */

namespace App\Twitter\Domain\Model\Tweet;

/**
 * Class TweetUsername
 * @package App\Twitter\Domain\Model\Tweet
 */
class TweetUsername
{
    const MIN_LENGTH = 3;
    const MAX_LENGTH = 15;

    /**
     * @var string
     */
    private $value;

    /**
     * UserName constructor.
     * @param $username
     */
    public function __construct(string $username)
    {
        $this->setValue(trim($username));
    }

    public function __toString()
    {
        return $this->getValue();
    }

    public function getValue()
    {
        return $this->value;
    }

    private function setValue(string $username)
    {
        $this->assertNotEmpty($username);
        $this->assertFitsLength($username);
        $this->assertIsValid($username);
        $this->value = $username;
    }

    private function assertNotEmpty($username)
    {
        if (empty($username)) {
            throw new \DomainException('Username must not be empty');
        }
    }

    private function assertFitsLength($username)
    {
        if (strlen($username) < self::MIN_LENGTH) {
            throw new \DomainException('Username is too sort');
        }
        if (strlen($username) > self::MAX_LENGTH) {
            throw new \DomainException('Username is too long');
        }
    }

    private function assertIsValid($username)
    {
        if(!preg_match("/^[_a-zA-Z0-9]+$/", $username)) {
            throw new \DomainException('Username can only contain alphanumeric characters (letters from A to Z, numbers from 0 to 9 and low guide)');
        }
    }
}