import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {environment} from "../../environments/environment";

import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, map, tap } from 'rxjs/operators';

import { Tweet } from '../domain/model/tweet';

const httpOptions = {
    headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
        'Accept': 'application/json'
    })
};



@Injectable()
export class ApiService {

    private baseUrl = environment.apiUrl;  // URL to web api

    constructor(
        private http: HttpClient) { }

    /** GET tweetes from the server */
    getTweetes (): Observable<Tweet[]> {
        const url = this.baseUrl + '/tweetes';
        return this.http.get<Tweet[]>(url)
            .pipe(
                tap(tweetes => this.log('fetched tweetes')),
                catchError(this.handleError('getTweetes', []))
            );
    }

    /** GET tweet by id. Return `undefined` when id not found */
    getTweetNo404<Data>(id: number): Observable<Tweet> {
        const url = `${this.baseUrl}/?id=${id}`;
        return this.http.get<Tweet[]>(url)
            .pipe(
                map(tweetes => tweetes[0]), // returns a {0|1} element array
                tap(h => {
                    const outcome = h ? `fetched` : `did not find`;
                    this.log(`${outcome} tweet id=${id}`);
                }),
                catchError(this.handleError<Tweet>(`getTweet id=${id}`))
            );
    }

    /** GET tweet by id. Will 404 if id not found */
    getTweet(id: number): Observable<Tweet> {
        const url = this.baseUrl + '/tweetes/' + id;
        return this.http.get<Tweet>(url).pipe(
            tap(_ => this.log(`fetched tweet id=${id}`)),
            catchError(this.handleError<Tweet>(`getTweet id=${id}`))
        );
    }

    /* GET tweetes whose name contains search term */
    searchTweetes(username: string, limit: number): Observable<Tweet[]> {
        if (!username.trim()) {
            // if not search term, return empty User array.
            return of([]);
        }
        const url = this.baseUrl + 'tweets/'+username+'/'+limit;
        return this.http.get<Tweet[]>(url).pipe(
            tap(_ => this.log(`found tweetes matching "${username}"`)),
            catchError(this.handleError<Tweet[]>('searchTweetes', []))
        );
    }

    //////// Save methods //////////

    /** POST: add a new tweet to the server */
    addTweet (tweet: Tweet): Observable<Tweet> {
        return this.http.post<Tweet>(this.baseUrl, tweet, httpOptions).pipe(
            tap((tweet: Tweet) => this.log(`added tweet w/ id=${tweet.id}`)),
            catchError(this.handleError<Tweet>('addTweet'))
        );
    }

    /** DELETE: delete the tweet from the server */
    deleteTweet (tweet: Tweet | number): Observable<Tweet> {
        const id = typeof tweet === 'number' ? tweet : tweet.id;
        const url = `${this.baseUrl}/${id}`;

        return this.http.delete<Tweet>(url, httpOptions).pipe(
            tap(_ => this.log(`deleted tweet id=${id}`)),
            catchError(this.handleError<Tweet>('deleteTweet'))
        );
    }

    /** PUT: update the tweet on the server */
    updateTweet (tweet: Tweet): Observable<any> {
        return this.http.put(this.baseUrl, tweet, httpOptions).pipe(
            tap(_ => this.log(`updated tweet id=${tweet.id}`)),
            catchError(this.handleError<any>('updateTweet'))
        );
    }

    /**
     * Handle Http operation that failed.
     * Let the app continue.
     * @param operation - name of the operation that failed
     * @param result - optional value to return as the observable result
     */
    private handleError<T> (operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {

            // TODO: send the error to remote logging infrastructure
            console.error(error); // log to console instead

            // TODO: better job of transforming error for user consumption
            this.log(`${operation} failed: ${error.message}`);

            // Let the app keep running by returning an empty result.
            return of(result as T);
        };
    }

    /** Log a TweetService message with the MessageService */
    private log(message: string) {
        // this.messageService.add('TweetService: ' + message);
    }

}
