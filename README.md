# Docker Symfony Angular letShout Test

Docker symfony gives you everything you need for developing Symfony application. This complete stack run with docker and [docker-compose](https://docs.docker.com/compose/).

## Installation fast

First of all, the first thing you should do is configure the ip for the mysql database, edit the following file:

    $ cp .env.dist .env && nano .env

Edit with your Twitter token access the following line:

    #TWITTER
    APP_TOKEN=YOUR-TWITTER-TOKEN
    APP_TSECRET=YOUR-TWITTER-TOKEN-SECRET
    APP_CKEY=YOUR-TWITTE-CONSUMER-KEY
    APP_CSECRET=YOUR-TWITTE-CONSUMER-SECRET

Now execute the next command in your console:

    $ sh deploy.sh
    
Enjoy :-)


## Installation step by step

1. Install [docker](https://docs.docker.com/compose/install/) and [docker-compose](https://docs.docker.com/compose/install/#install-compose)

2. Build/run containers with (with and without detached mode)

    ```bash
    $ docker-compose build
    $ docker-compose up -d
    ```

3. Update your system host file (add letshout.com)

    ```bash
    # UNIX only: get containers IP address and update host (replace IP according to your configuration) (on Windows, edit C:\Windows\System32\drivers\etc\hosts)
    $ sudo echo $(docker network inspect bridge | grep Gateway | grep -o -E '[0-9\.]+') "letshout.com" >> /etc/hosts
    ```

    **Note:** For **OS X**, please take a look [here](https://docs.docker.com/docker-for-mac/networking/) and for **Windows** read [this](https://docs.docker.com/docker-for-windows/#/step-4-explore-the-application-and-run-examples) (4th step).

4. Prepare Backend app

    1. Go to root directory
    2. Update .env

        ```
        # .env
        #TWITTER
        APP_TOKEN=YOUR-TWITTER-TOKEN
        APP_TSECRET=YOUR-TWITTER-TOKEN-SECRET
        APP_CKEY=YOUR-TWITTE-CONSUMER-KEY
        APP_CSECRET=YOUR-TWITTE-CONSUMER-SECRET
        ```

    3. Composer install & create database

        ```bash
        $ docker-compose exec site bash
        $ composer install
        ```
5. Prepare assets app
    
    1. Go to assets directory 
    2. Install nvm 
        ```
        curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.8/install.sh | bash
        ```
    3. Install Angular cli
        ```
        npm install -g @angular/cli
        ```
    4. Install all libraries javascript 
        ```
        npm install
        ```
    5. Compile the project
        ```
        ng build
        ```

6. Enjoy :-)

* Visit [letshout.com](http://letshout.com)  


## Usage

Just run `docker-compose up -d`, then:

* App: visit [letshout.com](http://letshout.com)  

## How it works?

Have a look at the `docker-compose.yml` file, here are the `docker-compose` built images:

* `db`: This is the MySQL database container,
* `site`: This is the PHP-FPM and Apache2 container in which the application volume is mounted,

This results in the following running containers:

```bash
$ docker-compose ps

       Name                     Command               State              Ports            
------------------------------------------------------------------------------------------
site-apache   docker-php-entrypoint /bin ...   Up      5233/tcp, 0.0.0.0:80->80/tcp
```

## Useful commands

```bash
# bash commands
$ docker-compose exec site bash

# Composer (e.g. composer update)
$ docker-compose exec site composer update

# Symfony commands
$ docker-compose exec site /var/www/html/bin/console cache:clear # Symfony4
$ docker-compose exec site bash
$ php bin/console cache:clear

# Retrieve an IP Address (here for the nginx container)
$ docker inspect --format '{{ .NetworkSettings.Networks.dockersymfony_default.IPAddress }}' $(docker ps -f name=nginx -q)
$ docker inspect $(docker ps -f name=site -q) | grep IPAddress

# F***ing cache/logs folder
$ sudo chmod -R 777 var/cache var/log var/sessions # Symfony4

# Check CPU consumption
$ docker stats $(docker inspect -f "{{ .Name }}" $(docker ps -q))

# Delete all containers
$ docker rm $(docker ps -aq)

# Delete all images
$ docker rmi $(docker images -q)
```

## FAQ

* Got this error: `ERROR: Couldn't connect to Docker daemon at http+docker://localunixsocket - is it running?
If it's at a non-standard location, specify the URL with the DOCKER_HOST environment variable.` ?  
Run `docker-compose up -d` instead.

* Permission problem? See [this doc (Setting up Permission)](http://symfony.com/doc/current/book/installation.html#checking-symfony-application-configuration-and-setup)

* How to config Xdebug?
Xdebug is configured out of the box!
Just config your IDE to connect port  `9001` and id key `PHPSTORM`



# Assets

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.6.4.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be sited in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
